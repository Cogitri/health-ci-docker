DOCKER ?= podman

all: build push

build:
	$(DOCKER) build --pull-always -t registry.gitlab.gnome.org/cogitri/health-ci-docker/health-ci:latest .

push:
	$(DOCKER) push registry.gitlab.gnome.org/cogitri/health-ci-docker/health-ci:latest
