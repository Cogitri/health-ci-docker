FROM fedora:35

RUN dnf install -y tracker3-devel gtk4-devel meson git xorg-x11-server-Xvfb \
    gobject-introspection-devel gcc gcc-c++ libadwaita
RUN curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs -o rustup.sh \
    && sh ./rustup.sh -y \
    && rm rustup.sh
RUN source $HOME/.cargo/env \
    && rustup component add clippy \
    && rustup toolchain install nightly \
    && rustup component add llvm-tools-preview --toolchain nightly \
    && cargo install cargo-llvm-cov
